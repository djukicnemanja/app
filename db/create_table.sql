create table tasks
(
  task_id         int auto_increment
    primary key,
  task_name       varchar(60)                 null,
  description     varchar(255)                null,
  created_user_id int                         null,
  share_user_id   json                        null,
  created_time    datetime                    null,
  edited_time     datetime                    null,
  complete        enum ('1', '0') default '0' null
);

create table users
(
  user_id   int auto_increment
    primary key,
  full_name varchar(120) null,
  email     varchar(255) not null,
  password  varchar(255) null,
  constraint users_email_uindex
  unique (email)
);

