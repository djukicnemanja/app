<?php

namespace todolist;

/**
 * Class HomeController
 * @package todolist
 */
class HomeController extends BaseController
{
    /**
     * Home Page
     */
    public function index(){
        $task = new TaskModel();

        $this->page->start_page();
        HomePageView::index($task->read_my_tasks($this->user));
        $this->page->end_page();
    }

}