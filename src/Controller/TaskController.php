<?php

namespace todolist;

/**
 * Class TaskController
 * @package todolist
 */
class TaskController extends BaseController
{

    /**
     * Page for add new task
     */
    public function add(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $task = new TaskModel($_POST);
            if($task->add()){
                MsgModel::addMsg("You have successfully add new task.");
                $this->redirect('/');
            }else{
                $this->print_err($task->error);
                $this->page->start_page();
                TaskView::add_task($task->table);
                $this->page->end_page();
            }
        }else{
            $this->page->start_page();
            TaskView::add_task();
            $this->page->end_page();
        }

    }

    /**
     * Page for edit task
     * @param $id
     */
    public function edit($id){
        $task = new TaskModel();

        if($task->read($id)){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $task = new TaskModel($_POST);
                if($task->edit()){
                    MsgModel::addMsg("You have successfully modified task.");
                    $this->redirect('/');
                }else{
                    $this->print_err($task->error);
                    $this->page->start_page();
                    TaskView::edit_task($task->table);
                    $this->page->end_page();
                }

            }else{
                $this->page->start_page();
                TaskView::edit_task($task->table);
                $this->page->end_page();
            }
        }else{
            $this->print_err("Task not found.");
            $this->redirect('/');
        }
    }

    /**
     * Page for delete task
     */
    public function delete($id){
        $task = new TaskModel();

        if($task->read($id)){
            if($task->delete()){
                MsgModel::addMsg("You have successfully deleted task.");
                $this->redirect('/');
            }
        }else{
            $this->print_err("Task not found.");
            $this->redirect('/');
        }

    }

}