<?php

namespace todolist;

/**
 * Class BaseController
 * @package todolist
 */
abstract class BaseController
{

    public $user;
    public $page;

    /**
     * Check if user is logged and redirect
     * BaseController constructor.
     */
    public function __construct()
    {
        if($this->if_user_is_looged()){
            $user = new UserModel();
            $this->user = $user->read_user($_SESSION['user_id']);
            $this->page = new PageBuilder($this->user);
        }else{
            $this->redirect('/login');
        }

    }


    /**
     * Function return true if user is logged in or false if not
     * @return bool
     */
    protected function if_user_is_looged(){
        if(isset($_SESSION['user_id'])) {
            return true;
        }
        return false;
    }

    /**
     * Function for redirecting user to $url page
     * @param $url
     */
    protected function redirect($url)
    {
        header('Location: https://' . $_SERVER['HTTP_HOST'] . $url, true, 303);
        exit;
    }

    /**
     * Function add err to msg
     * @param $error
     */
    protected function print_err($error){
        if (is_string($error)) $error = array($error);
        $html = '';
        foreach ($error as $item) {
            $html .=$item."<br>";
        }
        MsgModel::addMsg($html, MsgModel::DANGER);
    }

}