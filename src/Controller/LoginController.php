<?php

namespace todolist;

/**
 * Class LoginController
 * @package todolist
 */
class LoginController extends BaseController
{

    /**
     * Overrides __construct in BaseController,
     * LoginController constructor.
     */
    public function __construct()
    {
    }

    /**
     * Page for Login
     */
    public function login(){
        if(isset($_POST['email']) && isset($_POST['pass'])){
            $auth = new Auth();
            if($auth->login($_POST['email'], $_POST['pass'])){
                $this->redirect('/');

            }else{
                LoginView::login_form($auth->error);
            }
        }else{
           LoginView::login_form();
        }
    }

    /**
     * Page for logout
     */
    public function logout(){
        $auth = new Auth();
        $auth->logout();
        $this->redirect('/');
    }

}