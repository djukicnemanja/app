<?php

namespace todolist;


class Auth extends BaseModel
{

    /**
     * @param $email
     * @param $pass
     * @return bool
     */
    public function login($email, $pass){
        $email = $this->db->real_escape_string($email);
        $pass = $this->db->real_escape_string($pass);

        $sql = "SELECT user_id, password FROM users WHERE email='{$email}'";
        $user = $this->db->query($sql)->fetch_object();

        //check user
        if($user){
            //check password
            if(password_verify($pass, $user->password)){
                $this->start_session($user->user_id);
                return true;
            }else{
                $this->error[]= "Wrong password.";
            }
        }else{
            $this->error[] = "Email doesn't exist, check your email or create new account.";
        }
        return false;
    }

    /**
     * Fuction for remove session
     */
    public function logout(){
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
        session_destroy();
    }

    /**
     * @param $user_id
     */
    protected function start_session($user_id){
        session_regenerate_id(true);
        $_SESSION["user_id"] = $user_id;
    }

}