<?php

namespace todolist;

/**
 * Class UserModel
 * @package todolist
 */
class UserModel extends BaseModel
{

    /**
     * @param $id
     * @return object|\stdClass
     */
    public function read_user($id){
        $id = $this->db->real_escape_string($id);
        $sql = "SELECT user_id, full_name, email FROM users WHERE user_id = '{$id}'";
        return $this->db->query($sql)->fetch_object();
    }

    /**
     * @return array
     */
    public static function get_all_users(){
        $db = Database::connection();
        $sql = "SELECT * FROM users ORDER BY full_name";
        $res = $db->query($sql);

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_object()) {
                $all_user[] = $row;
            }
            return $all_user;
        }
        return array();
    }

    /**
     * Function return name from id
     * @param $id
     * @return mixed
     */
    public static function get_name_from_id($id){
        $db = Database::connection();
        $id = $db->real_escape_string($id);
        $sql = "SELECT full_name FROM users WHERE user_id='{$id}'";
        return $db->query($sql)->fetch_row()[0];
    }

}