<?php

namespace todolist;

/**
 * Class BaseModel
 * @package todolist
 */
abstract class BaseModel
{

    public $error;

    /**
     * @var \mysqli
     */
    public $db;

    function __construct(){
        $this->init_db();
    }

    function init_db(){
        $this->db = Database::connection();
    }

}