<?php

namespace todolist;

/**
 * Class TaskModel
 * @package todolist
 */
class TaskModel extends BaseModel
{
    public $table;

    /**
     * TaskModel constructor.
     * @param array $post
     */
    public function __construct($post = array())
    {
        parent::__construct();
        $this->table = new \stdClass();

        foreach ($post as $key => $value){
            if(is_array($value)){
                foreach ($value as $val){
                    $this->table->$key[] = $this->db->real_escape_string($val);
                }
            }else{
                $this->table->$key = $this->db->real_escape_string($value);
            }
        }
    }

    /**
     * @param $id
     * @return object|\stdClass
     */
    public function read($id){
        $id = $this->db->real_escape_string($id);
        $sql ="SELECT * FROM tasks WHERE task_id='{$id}'";

        $row = $this->db->query($sql);
        $this->table =$row->fetch_object();

        //json decode
        $this->table->share_user_id = json_decode($this->table->share_user_id);
        return $this->table;
    }

    /**
     * Add task to DB
     */
    public function add(){
        $this->validation();

        if(empty($this->error)){
            $sql = "INSERT INTO tasks SET
					created_user_id = '{$_SESSION['user_id']}',
					share_user_id = '{$this->table->share_user_id}',
					task_name = '{$this->table->task_name}',
					description = '{$this->table->description}',
					created_time = now()";
            return $this->db->query($sql);
        }
        return false;
    }

    /**
     * @return bool|\mysqli_result
     */
    public function edit(){
        $this->validation();

        if(empty($this->error)){
            $sql = "UPDATE tasks SET
					task_name = '{$this->table->task_name}',
					description = '{$this->table->description}',
					share_user_id = '{$this->table->share_user_id}',
					complete = '{$this->table->complete}',
					edited_time = now()
			    	WHERE task_id='{$this->table->task_id}'";
            return $this->db->query($sql);
        }
        return false;
    }

    /**
     * Function for delete task in DB
     * @return bool|\mysqli_result
     */
    public function delete(){
        $this->table->task_id = $this->db->real_escape_string($this->table->task_id);
        $sql = "DELETE FROM tasks WHERE task_id='{$this->table->task_id}' LIMIT 1";
        return $this->db->query($sql);
    }

    /**
     * validation function
     */
    private function validation(){

        // Task name
        if ($this->table->task_name == ''){
            $this->error[]="Task Name is required.";
        }
        // Task description
        if ($this->table->description == ''){
            $this->error[]="Task Description is required.";
        }

        // Task Status
        if(isset($this->table->complete)){
            $this->table->complete = 1;
        }else{
            $this->table->complete = 0;
        }

        // Crete JSON format
        if(empty($this->table->share_user_id)){
            $this->table->share_user_id = json_encode(array('0'), true);
        }else{
            $this->table->share_user_id = json_encode($this->table->share_user_id, true);
        }
    }

    /**
     * @param $user
     * @return array
     */
    public function read_my_tasks($user){
        $sql = "SELECT * FROM tasks WHERE created_user_id='{$user->user_id}' OR (JSON_SEARCH(share_user_id, 'all', '{$user->user_id}') IS NOT NULL) ORDER BY created_time DESC ";
        $row = $this->db->query($sql);

        if($row->num_rows > 0){
            while ($res = $row->fetch_object()){
                $my_tasks[] = $res;
            }
            return $my_tasks;
        }
        return array();
    }

}