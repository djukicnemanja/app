<?php

namespace todolist;


class MsgModel
{
    const SUCCESS = 'success';
    const INFO = 'info';
    const WARNING = 'warning';
    const DANGER = 'danger';

    /**
     * Function for add msg in session
     * @param $msg
     * @param string $type
     */
    public static function addMsg($msg, $type = 'success'){
        if(!isset($_SESSION['notice'])){
            $_SESSION['notice'] = [];
        }
        $_SESSION['notice'][] = ['text' => $msg, 'type' => $type];
    }

    /**
     *  Function return message and delete message
     * @return array
     */
    public static function getMsg(){
        if(isset($_SESSION['notice'])){
            $msg = $_SESSION['notice'];
            unset($_SESSION['notice']);
            return $msg;
        }else{
            return array();
        }
    }
}