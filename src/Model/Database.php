<?php

namespace todolist;

/**
 * Class Database
 * @package todolist
 */
class Database
{

    /**
     * @var /mysqli
     */
    public static $connection;

    /**
     * Function to get active connection
     * @return /mysqli
     */
    public static function connection(){
        // return connection
        if (self::$connection){
            return self::$connection;
        } else {
            // connect or die
            self::$connection = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME)
            or die(self::$connection->connect_error);
            self::$connection->set_charset("utf8");
            return self::$connection;
        }
    }

}