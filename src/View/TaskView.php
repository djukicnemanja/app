<?php
namespace todolist;

/**
 * Class TaskView
 * @package todolist
 */
class TaskView
{

    /**
     *
     */
    public static function add_task($task= false){
        ?>
        <div class="col-sm-6 mx-auto">
            <form method="post">
                <input name="task_id" type="hidden" value="<?=@$task->task_id?>">
                <div class="form-group">
                    <label for="task">Task Name</label>
                    <input type="text" name="task_name" class="form-control" id="task" placeholder="Task Name" value="<?=@$task->task_name?>">
                </div>
                <div class="form-group">
                    <label for="desc">Description</label>
                    <textarea name="description" class="form-control" id="desc" rows="3"><?=@$task->description?></textarea>
                </div>
                <div class="form-group">
                    <label for="shtask">Share task with</label>
                    <select class="custom-select" name="share_user_id[]" id="shtask" multiple="">
                        <?
                        foreach (UserModel::get_all_users() as $item) {
                            ?>
                            <option value="<?=$item->user_id?>" <?=(@in_array($item->user_id, $task->share_user_id))  ? "selected" : "" ?>><?=$item->full_name?></option>
                            <?
                        }
                        ?>
                    </select>
                </div>
                <hr/>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add task</button>
                </div>
            </form>
        </div>
        <?
    }


    /**
     * @param $task
     */
    public static function edit_task($task){
        ?>
        <div class="col-sm-6 mx-auto">
            <form method="post">
                <input name="task_id" type="hidden" value="<?=$task->task_id?>">
                <div class="form-group">
                    <label for="task">Task Name</label>
                    <input type="text" name="task_name" class="form-control" id="task" placeholder="Task Name" value="<?=$task->task_name?>">
                </div>
                <div class="form-group">
                    <label for="desc">Description</label>
                    <textarea name="description" class="form-control" id="desc" rows="3"><?=$task->description?></textarea>
                </div>
                <div class="form-group">
                    <label for="shtask">Share task with</label>
                    <select class="custom-select" name="share_user_id[]" id="shtask" multiple="">
                        <?
                        foreach (UserModel::get_all_users() as $item) {
                            ?>
                            <option value="<?=$item->user_id?>" <?=(@in_array($item->user_id, $task->share_user_id))  ? "selected" : "" ?>><?=$item->full_name?></option>
                            <?
                        }
                        ?>
                    </select>
                </div>
                <div class="form-check">
                    <input name="complete" <?=($task->complete == 1)? 'checked' : ''?> type="checkbox" class="form-check-input" id="comp">
                    <label class="form-check-label" for="comp">Complete</label>
                </div>
                <hr/>
                <div class="form-group">
                    <label for="crreated_id">Created by <?=UserModel::get_name_from_id($task->created_user_id)?> on <?=$task->created_time?></label>
                    <input name="created_user_id" type="hidden" class="form-control" id="crreated_id" value="<?=$task->created_user_id?>">
                </div>
                <div class="form-group">
<!--                    <label for="time">Created time</label>-->
                    <input name="created_time" type="hidden" class="form-control" id="time" value="<?=$task->created_time?>">
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                         Edit task</button>
                </div>
            </form>
        </div>
        <?
    }

}