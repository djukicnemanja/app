<?php
/**
 * Created by PhpStorm.
 * User: nemanja
 * Date: 16.9.19.
 * Time: 21.04
 */

namespace todolist;

/**
 * Class PageBuilder
 * @package todolist
 */
class PageBuilder
{

    public $user;

    public function __construct($user)
    {
        $this->user = $user;

    }

    public function start_page(){

        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

            <title>To-Do App | ZIRO</title>
        </head>
        <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <a class="navbar-brand" href="/">To-Do App</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto px-5">
                    <li class="nav-item text-white">
                        Hello, <b><?=$this->user->full_name?></b>
                    </li>
                </ul>
                <a href="/logout">
                    <button class="btn btn-danger my-2 my-sm-0" type="button">Log out</button>
                </a>
            </div>
        </nav>
            <main role="main" class="container">
        <?
        $this->print_msg();
    }

    public function end_page(){
        ?>

            </main>
            <footer class="footer mt-auto py-3">
                <div class="container text-center">
                    <span class="text-muted">Creatred for ZIRO - by Nemanja Dukic</span>
                </div>
            </footer>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/js/main.js"></script>
        </body>
        </html>
        <?
    }

    /**
     * Function for printing messages
     */
    public function print_msg(){
        $msg = MsgModel::getMsg();
        foreach ($msg as $item) { ?>
            <div class="m-2">
                <div class="alert alert-<?= $item['type']?> alert-dismissible fade show" role="alert">
                    <?= $item['text']?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?
        }
    }

}