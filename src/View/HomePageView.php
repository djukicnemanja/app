<?php

namespace todolist;

/**
 * Class HomePageView
 * @package todolist
 */
class HomePageView
{


    /**
     *
     */
    public static function index($my_task){
        ?>
        <div class="pull-right my-2">
            <a href="/task/add"><button class="btn btn-outline-success"><i class="fa fa-plus" aria-hidden="true"></i> Add new task</button></a>
        </div>

        <table class="table table-hover">
            <thead>
            <tr>
                <th colspan="6" class="text-center">My tasks</th>
            </tr>
            <tr>
                <th scope="col"></th>
                <th scope="col">Status</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Time</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?
            $i=1;
            foreach ($my_task as $item) {
                ?>
                <tr>
                    <th><?=$i?></th>
                    <td><?=($item->complete == "1")? "<p class='text-success'>Done</p>":"<p class='text-info'>In progress</p>"?></td>
                    <td><?=$item->task_name?></td>
                    <td><?=$item->description?></td>
                    <td><?=$item->created_time?></td>
                    <td>
                        <a class='btn btn-outline-primary btn-sm mx-1' role='button' href='/task/edit/<?=$item->task_id?>'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a class='btn btn-outline-danger btn-sm mx-1 task-del' role='button' href='/task/delete/<?=$item->task_id?>'><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?
                $i++;
            }
            ?>
            </tbody>
        </table>
        <?
    }
}