<?php

namespace todolist;

/**
 * Class LoginView
 * @package todolist
 */
class LoginView
{

    /**
     *
     */
    public static function login_form($error = array()){
        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

            <title>To-Do App | ZIRO</title>
        </head>
        <body>
        <main role="main" class="container">
            <div class="col-sm-6 mx-auto my-5 text-center">
                <form method="post">
                    <h2>Login form</h2>
                    <div class="form-group">
                        <input required name="email" type="email" class="form-control" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <input required name="pass" type="password" class="form-control" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
            <?
                foreach ($error as $item) {
                    ?>
                    <div class="m-2">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?= $item ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <?
                }
            ?>
            <div class="col-sm-6 mx-auto my-5 text-center">
                <h4>Demo users</h4>
                <table class="table">
                    <tr>
                        <td><b>Email</b></td>
                        <td><b>Pass</b></td>
                    </tr>
                    <tr>
                        <td>nemanja.djukich@gmail.com</td>
                        <td>123456</td>
                    </tr>
                    <tr>
                        <td>user@1.com</td>
                        <td>123456</td>
                    </tr>
                    <tr>
                        <td>user@2.com</td>
                        <td>123456</td>
                    </tr>
                    <tr>
                        <td>user@3.com</td>
                        <td>123456</td>
                    </tr>
                    <tr>
                        <td>user@4.com</td>
                        <td>123456</td>
                    </tr>
                </table>
            </div>
        </main>
        <footer class="footer mt-auto py-3">
            <div class="container text-center">
                <span class="text-muted">Creatred for ZIRO - by Nemanja Dukic</span>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/js/main.js"></script>
        </body>
        </html>
    <?
    }

}