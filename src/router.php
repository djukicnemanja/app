<?php

$dispicher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {

    $r->addRoute('GET', '', 'todolist\HomeController/index');
    $r->addRoute(['GET', 'POST'], '/login', 'todolist\LoginController/login');
    $r->addRoute(['GET', 'POST'], '/logout', 'todolist\LoginController/logout');

    $r->addGroup('/task', function (FastRoute\RouteCollector $r){
        $r->addRoute(['GET', 'POST'], '/add', 'todolist\TaskController/add');
        $r->addRoute(['GET', 'POST'], '/edit/{id:[0-9]+}', 'todolist\TaskController/edit');
        $r->addRoute(['GET', 'POST'], '/delete/{id:[0-9]+}', 'todolist\TaskController/delete');
    });

});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// If the url ends with slash delete the slash
if(substr($uri, -1) == '/'){
    $uri = substr_replace($uri ,"", -1);
}


if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);
$routeInfo = $dispicher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo "PAGE 404";
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo "METHOD NOT ALLOWED";
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);
        break;
}